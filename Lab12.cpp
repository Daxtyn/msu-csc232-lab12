/**
 * @file Lab12.cpp
 * @authors Jim Daehn, <Insert Your Names Here>
 * @brief Main driver for lab 12.
 */

#include <cstdlib>
#include <iomanip>
#include <iostream>
#include "BinaryNodeTree.h"

void display(std::string& anItem) {
	std::cout << std::setw(3) << anItem;
}  // end display

int main(int argc, char **argv) {
	auto tree1 = std::make_unique<BinaryNodeTree<std::string>>();

	tree1->add("10");
	tree1->add("20");
	tree1->add("30");
	tree1->add("40");
	tree1->add("50");
	tree1->add("60");
	tree1->add("70");
	tree1->add("80");

	std::cout << "Tree 1 Preorder traversal:\n";
	/*
	 The output should look like:

     Tree 1 Preorder traversal:
	  10 20 40 70 60 30 50 80

	 */
	tree1->preorderTraverse(display);

	std::cout << std::endl;

	std::cout << "Tree 1 Inorder traversal:\n";
	/*
	 The output should look like:

     Tree 1 Inorder traversal:
	  70 40 20 60 10 50 30 80

	 */
	tree1->inorderTraverse(display);

	std::cout << std::endl;

	std::cout << "Tree 1 Postorder traversal:\n";
	/*
	 The output should look like:

     Tree 1 Postorder traversal:
	  70 40 60 20 50 80 30 10

	 */
	tree1->postorderTraverse(display);

	std::cout << std::endl;

	return EXIT_SUCCESS;
}
