/*
 * NotFoundException.h
 *
 *  Created on: Dec 5, 2016
 *      Author: jdaehn
 */

#ifndef NOT_FOUND_EXCEPTION_H_
#define NOT_FOUND_EXCEPTION_H_

#include <string>
#include "CSC232CustomException.h"

class NotFoundException: public CSC232CustomException {
public:
	NotFoundException(const std::string& message = "");
};

#endif /* NOT_FOUND_EXCEPTION_H_ */
