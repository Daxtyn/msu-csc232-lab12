/*
 * CSC232CustomException.h
 *
 *  Created on: Dec 5, 2016
 *      Author: jdaehn
 */

#ifndef CSC232_CUSTOM_EXCEPTION_H_
#define CSC232_CUSTOM_EXCEPTION_H_

#include <stdexcept>
#include <string>

class CSC232CustomException : public std::logic_error {
public:
	CSC232CustomException(const std::string& message = "");
};

#endif /* CSC232_CUSTOM_EXCEPTION_H_ */
